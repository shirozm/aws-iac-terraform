variable "instance_type" {
    description = "EC2 instance type"
  }
 
variable "ami_id" {
    description = "Ec2 AMI ID"
  }
 
variable "subnet_id_value" {
  description = "ID of the subnet where the EC2 instance will be launched."
}
 
#variable "ingress_ports" {
#  type    = list(number)
#  default = [22, 80, 443]  # Define the list of ingress ports you want to allow
#}
variable "count_value" {
   description = "Ec2 instance count"
}
variable "ssh_port" {
  description = "Allowe security group for ssh"
}
 
variable "http_port" {
  description = "Allow http port"
}
 
variable "key_name_ssh" {
  description = "Allow authentication key for ssh"
}
