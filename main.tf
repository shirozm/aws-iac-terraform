resource "aws_security_group" "instance_sg" {
  name        = "instance_sg"
  description = "Security group for EC2 instance"
 
  ingress {
    from_port   = var.ssh_port
    to_port     = var.ssh_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Allowing SSH from anywhere
  }
 
  ingress {
    from_port   = var.http_port
    to_port     = var.http_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Allowing HTTP from anywhere
  }
 
  # Add more ingress blocks as needed
}
 
resource "aws_instance" "example" {
  ami = var.ami_id
  count = var.count_value
  instance_type = var.instance_type
  key_name = var.key_name_ssh
  subnet_id = var.subnet_id_value
  #vpc_security_group_ids = [aws_security_group.instance_sg.id]
 
  tags = {
      Name = "terraform-day3"
      Env = "dev"
      Description = "This is for testing terraform deployment"
  }
 
}
